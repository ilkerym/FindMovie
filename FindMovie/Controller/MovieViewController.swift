//
//  ViewController.swift
//  MovieSearch
//
//  Created by İlker Yasin Memişoğlu on 5.08.2022.
//

import UIKit

class MovieViewController: UIViewController {

    // outlet parameters
    @IBOutlet weak var searchControllerView: UIView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var movieTableView: UITableView!
    
    //other parameters
    let iParameter: String = ""
    let tParameter: String? = "Batman"
    let sParameter: String = ""


    var baseUrl: String {
        guard let tParameter = tParameter else {return "api tparameter error"}
        return "https://www.omdbapi.com/?t=\(String(describing: tParameter))&s=\(sParameter)&i=\(String(describing: iParameter))&apikey=fe4cf879"
    }
    var searchController = UISearchController(searchResultsController: nil)
    var movieManager = MovieManager()
    var movieArray = [MovieData]()
    var activityIndicator = UIActivityIndicatorView()
    var sampleArray = [1,2,3,4,5,6,7,8,9,10,11,12,15,34,56,90]
    var filteredMovie = [MovieData]()
    var isSearchBarEmpty: Bool {
      return searchController.searchBar.text?.isEmpty ?? true
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    // Do any additional setup after loading the view.
    // view.addSubview(searchController)
        movieTableView.register(MovieTableViewCell.self, forCellReuseIdentifier: "movieCell")
        movieTableView.delegate = self
        movieTableView.dataSource = self
        
        self.navigationController?.navigationBar.prefersLargeTitles = false
       
        let title = "Sinema"
        let font = UIFont(name: "AppleChancery", size: 20.0)
//      let font2 = UIFont(name: <#T##String#>, size: <#T##CGFloat#>)
        
        let attributes: [NSMutableAttributedString.Key: Any] = [.font: font, .foregroundColor: UIColor(ciColor: .black)]
        let attributedTitle = NSMutableAttributedString(string: title, attributes: attributes)
//      navigationItem.backButtonTitle = attributedTitle.string
        
        navigationController?.navigationBar.backgroundColor = .black
        // setting button
        let initialLabelButton = UIButton()
        initialLabelButton.setTitle("Sinema", for: .normal)
        initialLabelButton.backgroundColor = .none
        initialLabelButton.frame = CGRect(x: 0, y: 0, width: 60, height: 40)
        //setting Bar Button Item
        let initialLabelItem = UIBarButtonItem()
        initialLabelItem.customView = initialLabelButton
        self.navigationItem.leftBarButtonItem = initialLabelItem
        
        configureSearchControllerView()
   
        
     fetchMovieData(with: baseUrl)
     print(movieArray.count)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let selectedIndexPath = movieTableView.indexPathForSelectedRow {
            movieTableView.deselectRow(at: selectedIndexPath, animated: animated)
        }
    }
    private func showSpinner() {
        activityIndicator.style = .medium
        activityIndicator.color = .white
        activityIndicator.startAnimating()
        movieTableView.backgroundView = activityIndicator
    }
    private func removeSpinner() {
        activityIndicator.stopAnimating()
        activityIndicator.hidesWhenStopped = true
    }
    
    func configureSearchControllerView() {
    
       // searchController.automaticallyShowsScopeBar = true
        
        // 1
        searchController.searchResultsUpdater = self
        // 2
        searchController.obscuresBackgroundDuringPresentation = false
        // 3
        searchController.searchBar.placeholder = "Search Movies"
        // 4
        navigationItem.searchController = searchController
        // 5
        definesPresentationContext = true

    }
    
//MARK: - İçerik Filtreleme
    // Burdan devam filtreleme
//    func filterContentForSearchText(_ searchText: String, wholeNumber : Int? = nil) {
//
//        filteredSampleArray = sampleArray.filter { (candy: Int) -> Bool in
//          //  let doesCategoryMatch = wholeNumber == .all || candy.category == category
//
//            let doesNumberMatch =
//
//            if isSearchBarEmpty {
//              return doesNumberMatch
//            } else {
//              return doesNumberMatch //&& candy.name.lowercased()
//                .contains(searchText.lowercased())
//            }
//          }
//
//        //  movieTableView.reloadData()
//
//    }
//    
    
    func fetchMovieData(with urlString: String)  {
        
        if let url = URL(string: urlString) {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { data, response, error in
                
                if error != nil {
                    print("error")
                    return
                }
                if let data = data {
                    print("data")
                    if let movie = self.parseJSON(data){
                        print(movie.title)
                       
                        self.movieArray.append(movie)
                        print(self.movieArray[0].genre)
                        DispatchQueue.main.async {
                            self.movieTableView.reloadData()
                            print("movie table works")
                        }
                        
                  
                    }
                }
            }
            task.resume()
        }
    }
    func parseJSON(_ movieData: Data) -> MovieData? {
        let decoder = JSONDecoder()
        
        do {
            let movie = try decoder.decode(MovieModel.self, from: movieData)
            let header = movie.title
            let genre = movie.genre
            let plot = movie.plot
            let poster = movie.poster
            
            let movieData = MovieData(title: header, genre: genre, plot: plot, poster: poster)
        
            return movieData
            
        } catch {
            print(error.localizedDescription)
            return nil
        }
        
    }
    
}
extension MovieViewController: UISearchResultsUpdating {
    public func updateSearchResults(for searchController: UISearchController) {
        guard let query = searchController.searchBar.text else{return}
        print(query)
        
    }
}
//MARK: - Data Source Methods
extension MovieViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MovieTableViewCell
    
        
        cell.movie = movieArray[indexPath.row]
        
        
        print("before CELL ------> \(movieArray[indexPath.row].genre)")
        
        return cell
    }
   

}

//MARK: - Delegate Methods

extension MovieViewController: UITableViewDelegate {
    
//    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//
////        let cell = tableView.cellForRow(at: indexPath) as? MovieTableViewCell
////        let selectedMovie = movieArray[indexPath.row]
////
////        if let detailViewController = storyboard?.instantiateViewController(identifier: "DetailViewController") as? DetailViewController {
////            detailViewController.movie = selectedMovie
////
////            let imageString = selectedMovie.poster
////            let imageView = UIImageView()
////            imageView.load(url: URL(string: imageString)!)
////            detailViewController.movieImage = cell?.moviePosterImage // take image from the cell not to request again
////            navigationItem.backButtonTitle = selectedMovie.title
////            navigationItem.backBarButtonItem?.tintColor = .cyan
////            navigationController?.pushViewController(detailViewController, animated: true)
////           }
////
////        return indexPath
//    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true)
        
       // let cell = tableView.cellForRow(at: indexPath) as? MovieTableViewCell

        let selectedMovie = movieArray[indexPath.row]
        
        
        if let detailViewController = self.storyboard?.instantiateViewController(identifier: "DetailViewController") as? DetailViewController {
            detailViewController.movie = selectedMovie
            print("Genre---------------------> \(selectedMovie.plot)")

           
            
            let imageString = selectedMovie.poster
            let movieImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 300, height: 360))
            
            movieImageView.loadImage(imageString)
            movieImageView.contentMode = .scaleAspectFit
            let movieImage = movieImageView.image
            let resizedMovieImage = movieImage?.resized(to: CGSize(width: 300, height: 360))
            
            
            detailViewController.moviePlot = selectedMovie.plot
            detailViewController.movieGenre = selectedMovie.genre
            detailViewController.movieImage = resizedMovieImage // take image from the cell not to request again
            navigationItem.backButtonTitle = selectedMovie.title
            navigationController?.pushViewController(detailViewController, animated: true)
           }
        

//        let movie = cell?.movie
//        detailVC.movieGenre = movie?.genre
//        detailVC.moviePlot = movie?.plot
            // reloading image
//        let imageString = movie?.poster
//        let imageView = UIImageView()
//        imageView.load(url: URL(string: imageString!)!)
//        detailVC.movieImage = cell?.moviePosterImage  //imageView.image

            
            
//            destinationVC.charLargeImage = cell.charImageView.image // Because the size of image is different, I take image from cell
//            destinationVC.characterId = Int(character.id)
//            destinationVC.charName = character.charName
//            destinationVC.charDescription = character.charDescription
//            let comicUrl = "https://gateway.marvel.com/v1/public/characters/\(character.id)/comics"
//            destinationVC.comicUrl = comicUrl
        
//        navigationItem.backButtonTitle = movie?.title
//        navigationController?.pushViewController(detailVC, animated: true)
    }
    
}

//MARK: - Movie Manager Delegate
//extension MovieViewController : MovieManagerDelegate {
//    func didUpdateMovie(_ movieManager: MovieManager, movie: MovieData) {
//        DispatchQueue.main.async {
//
//            let indexPath = IndexPath()
//
//            let cell =  self.movieTableView.dequeueReusableCell(withIdentifier: "movieCell", for: indexPath) as! MovieTableViewCell
//
//
//            cell.movie = movie
//
//            movieManager.fetchMovieData(with: movieManager.baseUrl)
//            self.movieTableView.reloadData()
//        }
//    }
//
//    func didFailWithError(error: Error) {
//        print(error)
//    }
//
//
//}

