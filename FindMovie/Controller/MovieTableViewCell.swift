//
//  TableViewCell.swift
//  MovieSearch
//
//  Created by İlker Yasin Memişoğlu on 5.08.2022.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    var sampleCellInt = Int()
    var movie: MovieData?
    var moviePosterView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 150))
    
    var moviePosterImage = UIImage()
    let imageCache = NSCache<AnyObject, AnyObject>()
    override func updateConfiguration(using state: UICellConfigurationState) {
        super.updateConfiguration(using: state)

       
        if let movie = movie {
            
                var movieContent = defaultContentConfiguration()
               
           
            movieContent.text = movie.title
            movieContent.secondaryText = """
                                             \(String(describing: movie.genre)) \n
                                             \(String(describing: movie.plot))
                                             """
            // getting movie poster
            moviePosterView.loadImage(movie.poster)
            moviePosterView.contentMode = .scaleAspectFit
            let movieImage = moviePosterView.image
            let resizedMovieImage = movieImage?.resized(to: CGSize(width: 100, height: 150))
            movieContent.imageProperties.reservedLayoutSize = CGSize(width: 100, height: 150)
            movieContent.image = resizedMovieImage
            self.contentConfiguration = movieContent
            
            }
            
        }
    
}
//        let sampleAd = "İlker"
//        let boldFont = UIFont.boldSystemFont(ofSize: 12)
//        let attributes = [NSMutableAttributedString.Key.font: boldFont]
//        let attributedSampleAd = NSMutableAttributedString(string: sampleAd, attributes: attributes)
//
//        let sampleSoyAd = "Yasin"
//        let font = UIFont.systemFont(ofSize: 12)
//        let attribute = [NSMutableAttributedString.Key.font: font]
//        let attributedSampleSoyad = NSMutableAttributedString(string: sampleSoyAd, attributes: attribute)
//
//        var sampleContent = defaultContentConfiguration()
//        sampleContent.text = """
//                             \(String(sampleCellInt)) \n
//                             \(attributedSampleAd) \n
//                             \(attributedSampleSoyad)
//                             """
//        sampleContent.image = UIImage(systemName: "pencil")
//        self.contentConfiguration = sampleContent
