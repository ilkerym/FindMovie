//
//  DetailViewController.swift
//  MovieSearch
//
//  Created by İlker Yasin Memişoğlu on 7.08.2022.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var posterView: UIView!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var plotLabel: UILabel!
    
    var movieImage: UIImage?
    var movieGenre: String?
    var moviePlot: String?

    var movie: MovieData? {
        willSet {
        
            if let genre = movie?.genre, let plot = movie?.plot  {
                
                print("Heyy---------->\(genre)")
                movieGenre = genre
                moviePlot = plot
            }
        }
        didSet {
            movie = MovieData(title: "dummy", genre: "dummy", plot: "dummy", poster: "dummy")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        if let moviePlot = moviePlot, let movieGenre = movieGenre {
            print("detail geldi ----> \(moviePlot)")
            plotLabel.text = moviePlot
            genreLabel.text = movieGenre
        }
        
        if let movieImage = movieImage {
            posterImageView.image = movieImage
        }
        
        
        
       
    }
}
