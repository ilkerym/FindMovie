//
//  MovieModel.swift
//  MovieSearch
//
//  Created by İlker Yasin Memişoğlu on 5.08.2022.
//

import Foundation

// MARK: - MovieModel
//
//struct MovieModel : Codable {
//    let title, year, rated, released: String
//    let runtime, genre, director, writer: String
//    let actors, plot, language, country: String
//    let awards: String
//    let poster: String
//    let ratings: [Rating]
//    let metascore, imdbRating, imdbVotes, imdbID: String
//    let type, dvd, boxOffice, production: String
//    let website, response: String
//}
//
//// MARK: - Rating
//struct Rating : Codable {
//    let source, value: String
//}
// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let movieModel = try? newJSONDecoder().decode(MovieModel.self, from: jsonData)

import Foundation

// MARK: - MovieModel
struct MovieModel: Codable {
    let title : String
    let genre : String
    let plot: String
    let poster: String

    enum CodingKeys: String, CodingKey {
        case title = "Title"
       
        case genre = "Genre"
        
        case plot = "Plot"
        
        case poster = "Poster"
        
    }
}






// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:




