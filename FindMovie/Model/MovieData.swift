//
//  MovieData.swift
//  MovieSearch
//
//  Created by İlker Yasin Memişoğlu on 11.08.2022.
//

import Foundation

struct MovieData: Codable {
    var title: String
    var genre: String
    var plot: String
    var poster: String
}


