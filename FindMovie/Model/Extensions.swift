//
//  Extensions.swift
//  FindMovie
//
//  Created by ilkerym on 19.08.2022.
//

import UIKit

var imageCache = NSCache<NSString,UIImage>()

extension UIImageView {
 
    
    func loadImage (_ urlString: String) {
        if let cachedImage = imageCache.object(forKey: urlString as NSString) {
            self.image = cachedImage
            return
        }
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with:url) {
            (data,response,error) in
            if let error = error {
                print("Couln't download image: ", error)
                return
            }
            guard let data = data else { return }
            
            let image = UIImage(data: data)
            if let imageData = image {
                imageCache.setObject(imageData, forKey: urlString as NSString)
            }
            DispatchQueue.main.async {
                self.image = image
            }
        }
        .resume()
    }
    
}

extension UIImage {
    func resized(to size: CGSize) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }
    }
}
